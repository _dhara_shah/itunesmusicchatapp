package com.test.itunesmusicchartapp.models;

import java.util.List;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlRootElement;

/**
 * Song holds information related to the song
 * Such as the title of the song, the id, the link
 * Used by {@TopSongs}
 * @author USER
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="entry")
public class Song {
	@XmlElement(name="id",type=SongIdentification.class)
	private SongIdentification songIdentification;
	@XmlElement(name="title")
	private String title;
	@XmlElement(name="image",namespace="http://itunes.apple.com/rss")
	private String imgUrl;
	@XmlElement(name="link", type=SongLink.class)
	private List<SongLink> songLinks;
	
	public SongIdentification getSongIdentification() {
		return songIdentification;
	}
	public void setSongIdentification(SongIdentification songIdentification) {
		this.songIdentification = songIdentification;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public List<SongLink> getSongLinks() {
		return songLinks;
	}
	public void setSongLinks(List<SongLink> songLinks) {
		this.songLinks = songLinks;
	}
}
