package com.test.itunesmusicchartapp.models;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlAttribute;

/**
 * SongLink holds the details that makes the song link
 * Used to show details about the song in the webview
 * Used by {@Song}
 * @author USER
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SongLink {
	@XmlAttribute
	private String rel;
	@XmlAttribute
	private String type;
	@XmlAttribute
	private String href;
	
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRel() {
		return rel;
	}
	public void setRel(String rel) {
		this.rel = rel;
	}
}
