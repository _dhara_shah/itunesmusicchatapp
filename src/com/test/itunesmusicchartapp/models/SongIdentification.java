package com.test.itunesmusicchartapp.models;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlAttribute;
import ae.javax.xml.bind.annotation.XmlValue;

/**
 * Class that holds the song identification details
 * Such as the id of the song and also link that points to the song
 * @author USER
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SongIdentification {
	@XmlValue
	private String id;
	@XmlAttribute(name="id", namespace="http://itunes.apple.com/rss")
	private String uniqueId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
}
