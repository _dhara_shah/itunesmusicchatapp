package com.test.itunesmusicchartapp.models;

import java.util.ArrayList;
import java.util.List;

import ae.javax.xml.bind.annotation.XmlAccessType;
import ae.javax.xml.bind.annotation.XmlAccessorType;
import ae.javax.xml.bind.annotation.XmlElement;
import ae.javax.xml.bind.annotation.XmlRootElement;

/**
 * TopSongs holds the list of songs
 * returned by the xml call
 * @author USER
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="feed",namespace="http://www.w3.org/2005/Atom")
public class TopSongs {
	@XmlElement(name="entry", type=Song.class)
	private List<Song> songs  = new ArrayList<Song>();

	public List<Song> getSongs() {
		return songs;
	}

	public void setFood(List<Song> songs) {
		this.songs = songs;
	}
}
