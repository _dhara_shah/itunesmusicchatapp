package com.test.itunesmusicchartapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.MergeCursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.test.itunesmusicchartapp.adapters.SongCursorAdapter;
import com.test.itunesmusicchartapp.asynctasks.DownloadImageAsyncTask;
import com.test.itunesmusicchartapp.asynctasks.GetSongsAsyncTask;
import com.test.itunesmusicchartapp.contentprovider.SongContentProvider;
import com.test.itunesmusicchartapp.db.SongTable;

/**
 * The first screen that shows the list of songs
 * Items will be fetched through an async task
 * @author USER
 *
 */
public class MainActivity extends FragmentActivity implements LoaderCallbacks<Cursor>, OnItemClickListener, OnScrollListener{
	private SongCursorAdapter mSongCursorAdapter;
	private CursorLoader mCursorLoader;
	private Cursor mCursor;
	private ListView mSongListView;
	private TextView mTxtSaveImage, mTxtViewInfo;
	private int mLimitFrom;
	private SharedPreferences mSp;
	private Editor mEditor;
	private String[] projection;
	private Cursor mCursorLoadMore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// initialization of view(s)
		mSongListView = (ListView)findViewById(android.R.id.list);
		
		// delete the records already present
		// done this so as to clean the database, 
		// else repeated records will be present
		getContentResolver().delete(SongContentProvider.CONTENT_URI, null, null);
		mSp = PreferenceManager.getDefaultSharedPreferences(this);
		mLimitFrom = 0;
		storeLimitValue(mLimitFrom);
		
		// initialize the loader
		fillData();
		
		// make an async call to fetch the records
		// limit set to 20 in this case to show addition of new items on scroll
		new GetSongsAsyncTask(MainActivity.this, 20).execute();
		
		// listeners set on the listview
		mSongListView.setOnItemClickListener(this);
		mSongListView.setOnScrollListener(this);
	}
	
	/**
	 * Display message since the response was not received
	 */
	public void displayMessage(){
		Toast.makeText(this, getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		mLimitFrom = 0;
		storeLimitValue(mLimitFrom);
		
		// Fields from the database (projection)
		// Must include the _id column for the adapter to work
		projection = new String[]{ SongTable.COL_ID, 
				SongTable.COL_SONG_TITLE, SongTable.COL_SONG_LINK, SongTable.COL_ALBUM_IMG };
		mCursorLoader = new CursorLoader(MainActivity.this,
				SongContentProvider.CONTENT_URI, projection, null, null, null);
		return mCursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mCursor = cursor;
		mSongCursorAdapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mSongCursorAdapter.swapCursor(null);
	}

	/**
	 * Initializes the loader manager
	 * and also the list adapter
	 */
	private void fillData() {
		getSupportLoaderManager().initLoader(0, null, this);
		mSongCursorAdapter = new SongCursorAdapter(this,mCursor,false);
		mSongListView.setAdapter(mSongCursorAdapter);
	}
	
	/**
	 * OnItemClick the view will be made visible or invisible
	 * on visibility true, the id of the record selected will be stored
	 * and the extra options will be displayed (Save image, and view info)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		final Cursor cursor = mSongCursorAdapter.getCursor();
		cursor.moveToPosition(position);
		final String imgURL = cursor.getString(cursor.getColumnIndex(SongTable.COL_ALBUM_IMG));
		final String albumName = cursor.getString(cursor.getColumnIndex(SongTable.COL_SONG_TITLE));
		final String songLink = cursor.getString(cursor.getColumnIndex(SongTable.COL_SONG_LINK));
		final String _id = cursor.getString(cursor.getColumnIndex(SongTable.COL_ID));
		
		RelativeLayout relExtraOptions = (RelativeLayout)view.findViewById(R.id.relExtraOptions);
		TextView txtAlbumName = (TextView)view.findViewById(R.id.txtSongTitle);
		
		mTxtSaveImage = (TextView)view.findViewById(R.id.txtSaveImage);
		mTxtViewInfo = (TextView)view.findViewById(R.id.txtViewInfo);

		mTxtSaveImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				showMessage("",getString(R.string.download_started));
				new DownloadImageAsyncTask(MainActivity.this, imgURL, albumName).execute();
			}
		});

		mTxtViewInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, SongLinkActivity.class);
				intent.putExtra("songLink", songLink);
				startActivity(intent);
				overridePendingTransition(R.anim.activity_in_anim, R.anim.activity_out_anim);
			}
		});

		if(txtAlbumName.getText().toString().equalsIgnoreCase(albumName)) {
			if(relExtraOptions.getVisibility() == View.GONE) {
				// save the id to sharedpreferences
				// id of the item that was clicked on and made visible
				mEditor = mSp.edit();
				mEditor.putString("cellSelected", _id);
				mEditor.commit();
				
				relExtraOptions.setVisibility(View.VISIBLE);
			}else {
				// remove id from sharedpreferences if the view is made invisible
				if(mSp.contains("cellSelected")) {
					mEditor = mSp.edit();
					mEditor.remove("cellSelected");
					mEditor.commit();
				}
				relExtraOptions.setVisibility(View.GONE);
			}
		}else {
			// remove id from sharedpreferences if the view is made invisible
			if(mSp.contains("cellSelected")) {
				mEditor = mSp.edit();
				mEditor.remove("cellSelected");
				mEditor.commit();
			}
			relExtraOptions.setVisibility(View.GONE);
		}
	}

	/**
	 * Displays message as to whether download of image 
	 * has started, failed or completed successfully
	 * @param filePath
	 * @param message
	 */
	public void showMessage(String filePath, String message) {
		if(message == null) {
			if(filePath!=null) {
				Toast.makeText(MainActivity.this, getString(R.string.download_complete), Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(MainActivity.this, getString(R.string.download_not_complete), Toast.LENGTH_SHORT).show();
			}
		}else {
			Toast.makeText(MainActivity.this, getString(R.string.download_started), Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * When the count of items displayed and the value of the last record
	 * is the same, new records will be loaded from the cursor
	 */
	@Override
    public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
		// remove the id selected or clicked on while scroll starts
		if(mSp.contains("cellSelected")) {
			mEditor = mSp.edit();
			mEditor.remove("cellSelected");
			mEditor.commit();
		}
		
        if (firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0) {
            mLimitFrom = totalItemCount; 
            storeLimitValue(mLimitFrom);
            
            Log.i("dhara", " limit from value: " + mLimitFrom);
            
            projection = new String[]{ SongTable.COL_ID, 
    				SongTable.COL_SONG_TITLE, SongTable.COL_SONG_LINK, SongTable.COL_ALBUM_IMG };
            mCursorLoadMore = getContentResolver().query(SongContentProvider.CONTENT_URI, 
            		projection, null, null, null);
            
            // check if the value of count obtained is equal or more than the limit
            if(mCursorLoadMore != null && mCursorLoadMore.getCount() > 0) {
            	mLimitFrom = mLimitFrom + mCursorLoadMore.getCount();
            	storeLimitValue(mLimitFrom);
            	
            	Cursor[] cursors = new Cursor[] {mCursor, mCursorLoadMore};
                MergeCursor mergeCursor = new MergeCursor(cursors);
                mCursor = mergeCursor;
                
                mSongCursorAdapter.swapCursor(mCursor);
            }
        }
    }

	@Override
	public void onScrollStateChanged(AbsListView parent, int scrollState) {
		if (scrollState == OnScrollListener.SCROLL_STATE_IDLE ) {
           mSongListView.invalidateViews();
        }
	}
	
	/**
	 * Store the lower limit of the records for the next scroll update
	 * used in limit query as limit (offset), 10 (fixed number)
	 * @param offset
	 */
	private void storeLimitValue(int offset) {
		mEditor = mSp.edit();
		mEditor.putString("offset", String.valueOf(offset));
		mEditor.commit();
	}
}
