package com.test.itunesmusicchartapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Database helper class that creates the database
 * @author USER
 *
 */
public class DBHelper extends SQLiteOpenHelper{
	public final static String DATABASE_NAME = "TopSongs.sqlite";
	public final static int DATABASE_VERSION = 1;
	
	/**
	 * Constructor
	 * @param context
	 */
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		SongTable.onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		SongTable.onUpgrade(db, oldVersion, newVersion);
	}
}
