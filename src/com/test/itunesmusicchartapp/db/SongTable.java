package com.test.itunesmusicchartapp.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Class that holds the table related information and
 * the create and update query for the database creation and updation
 * @author USER
 *
 */
public class SongTable {
	public static final String TABLE_SONGS = "Songs";
	public static final String COL_SONG_TITLE = "song_title";
	public static final String COL_SONG_LINK = "song_link";
	public static final String COL_ALBUM_IMG = "song_album_img";
	public static final String COL_SONG_ID = "song_id";
	public static final String COL_ID = "_id";

	public static final String DATABASE_CREATE = "create table "
			+ TABLE_SONGS + "(" + COL_ID
			+ " integer primary key autoincrement, " + COL_SONG_TITLE
			+ " text , " + COL_SONG_LINK 
			+ " text , " + COL_ALBUM_IMG
			+ " text);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_SONGS);
		onCreate(database);
	}
}
