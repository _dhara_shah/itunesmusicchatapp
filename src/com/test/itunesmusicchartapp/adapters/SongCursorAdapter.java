package com.test.itunesmusicchartapp.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.test.itunesmusicchartapp.R;
import com.test.itunesmusicchartapp.db.SongTable;

public class SongCursorAdapter extends CursorAdapter{
	private Context mContext;
	private LayoutInflater mInflater;
	private View mView;
	private DisplayImageOptions mOptions;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	private SharedPreferences mSp;
	
	public SongCursorAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
		mContext = context;
		mSp = PreferenceManager.getDefaultSharedPreferences(mContext);
		
		mOptions = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.ic_stub)
			.showImageForEmptyUri(R.drawable.ic_stub)
			.showImageOnFail(R.drawable.ic_error)
			.cacheInMemory(true)
			.cacheOnDisc(true)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView txtSongTitle = (TextView)view.findViewById(R.id.txtSongTitle);
		ImageView imgSongAlbum = (ImageView)view.findViewById(R.id.imgSongAlbum);
		RelativeLayout relExtraOptions = (RelativeLayout)view.findViewById(R.id.relExtraOptions);
		
		if(cursor != null) {
			txtSongTitle.setText(cursor.getString(cursor.getColumnIndex(SongTable.COL_SONG_TITLE)));
			String imageUrl = cursor.getString(cursor.getColumnIndex(SongTable.COL_ALBUM_IMG));
			String _id = cursor.getString(cursor.getColumnIndex(SongTable.COL_ID));
			imageLoader.displayImage(imageUrl, imgSongAlbum, mOptions);
			
			if(mSp.getString("cellSelected","").trim().length() > 0) {
				if(mSp.getString("cellSelected", "").equals(_id)) {
					// the cell has been clicked on 
					// therefore leave the layout visible
					relExtraOptions.setVisibility(View.VISIBLE);
				}else {
					// hide the layout since it is not the one clicked on
					relExtraOptions.setVisibility(View.GONE);
				}
			}else{
				// hide the layout since it is not the one clicked on
				relExtraOptions.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.individual_list_item, parent, false);
		return mView;
	}
}
