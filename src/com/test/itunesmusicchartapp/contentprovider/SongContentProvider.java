package com.test.itunesmusicchartapp.contentprovider;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.test.itunesmusicchartapp.db.DBHelper;
import com.test.itunesmusicchartapp.db.SongTable;

/**
 * Class that performs database operations (CRUD)
 * using ContentProvider
 * @author USER
 *
 */
public class SongContentProvider extends ContentProvider{
	private static final String AUTHORITY = "com.test.itunesmusicchartapp.contentprovider";
	private static final String BASE_PATH = "topsongs";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
	public static final String CONTENT_TYPE=ContentResolver.CURSOR_DIR_BASE_TYPE;
	public static final String CONTENT_ITEM_TYPE=ContentResolver.CURSOR_ITEM_BASE_TYPE;
	private static final int TOPSONGS = 1;
	private static final int TOPSONGS_ID = 2;
	private static final UriMatcher mURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	static {
		mURIMatcher.addURI(AUTHORITY, BASE_PATH, TOPSONGS);
		mURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", TOPSONGS_ID);
	}
	private DBHelper mDbHelper;
	private String mOffset;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = mURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case TOPSONGS:
			rowsDeleted = sqlDB.delete(SongTable.TABLE_SONGS, selection,
					selectionArgs);
			break;
		case TOPSONGS_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(SongTable.TABLE_SONGS,
						SongTable.COL_ID + "=" + id, 
						null);
			} else {
				rowsDeleted = sqlDB.delete(SongTable.TABLE_SONGS,
						SongTable.COL_ID + "=" + id 
						+ " and " + selection,
						selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = mURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
		long id = 0;
		switch (uriType) {
		case TOPSONGS:
			id = sqlDB.insert(SongTable.TABLE_SONGS, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}
	
	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		int rowsInserted = 0;
		int uriType = mURIMatcher.match(uri);
		switch (uriType) {
		case TOPSONGS:
			SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
			sqlDB.beginTransaction();
			try {
				for (ContentValues contentValues : values) {
					long newID = sqlDB.insertOrThrow(SongTable.TABLE_SONGS, null, contentValues);
					if (newID <= 0) {
						throw new SQLException("Failed to insert row into " + uri);
					}
				}
				sqlDB.setTransactionSuccessful();
				getContext().getContentResolver().notifyChange(uri, null);
				rowsInserted = values.length;
			} finally {         
				sqlDB.endTransaction();
			}
			break;
		}
		return rowsInserted;
	}

	@Override
	public boolean onCreate() {
		mDbHelper = new DBHelper(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// Check if the caller has requested a column which does not exists
		checkColumns(projection);

		// Set the table
		queryBuilder.setTables(SongTable.TABLE_SONGS);
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		mOffset = sp.getString("offset","0");

		int uriType = mURIMatcher.match(uri);
		switch (uriType) {
		case TOPSONGS:
			// request made for all the songs
			break;
		case TOPSONGS_ID:
			// Adding the ID to the original query
			queryBuilder.appendWhere(SongTable.COL_ID + "="
					+ uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		
		StringBuffer columns = new StringBuffer();
		
		for(String string : projection) {
			columns.append(string + " , ");
		}
		columns.deleteCharAt(columns.length() - 2);
		
		// sql with limit in the query
		// since querybuilder does not support limits
		String sql = "select " + columns.toString() + " from " + SongTable.TABLE_SONGS 
				+ " limit " + mOffset + ", 5"; 
		
		// commented this line as this would return all the rows
		// but limit is implemented in order to show 
		// that new records are fetched on scroll of the list
		/*Cursor cursor = queryBuilder.query(db, projection, selection,
				selectionArgs, null, null, sortOrder);*/
		
		Cursor cursor = db.rawQuery(sql, selectionArgs);
		// Make sure that potential listeners are getting notified
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = mURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case TOPSONGS:
			rowsUpdated = sqlDB.update(SongTable.TABLE_SONGS, 
					values, 
					selection,
					selectionArgs);
			break;
		case TOPSONGS_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(SongTable.TABLE_SONGS, 
						values,
						SongTable.COL_ID + "=" + id, 
						null);
			} else {
				rowsUpdated = sqlDB.update(SongTable.TABLE_SONGS, 
						values,
						SongTable.COL_ID + "=" + id 
						+ " and " 
						+ selection,
						selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

	/**
	 * Checks that the columns requested for are valid columns
	 */
	private void checkColumns(String[] projection) {
		String[] available = { SongTable.COL_ALBUM_IMG,
				SongTable.COL_SONG_LINK, SongTable.COL_SONG_TITLE,
				SongTable.COL_ID };
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			// Check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}
