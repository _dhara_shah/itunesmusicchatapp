package com.test.itunesmusicchartapp.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

/**
 * Class for other operations
 * @author USER
 *
 */
public class Util {
	private Context mContext;
	private String mFilePath;
	
	/**
	 * Constructor
	 * @param context
	 */
	public Util(Context context) {
		mContext = context;
	}

	/**
	 * Downloads image from the url specified
	 * and stores the image in the sdcard or the downloads folder 
	 * of the phone if the sdcard is not present
	 * returns the path where the image is stored if successful 
	 * else returns a null value
	 * @param imgURL
	 * @param albumName
	 * @return
	 */
	public String downloadImageFromURL(String imgURL, String albumName) {
		try {   
			URL url = new URL(imgURL);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);                   
			urlConnection.connect();                  
			File SDCardRoot = null;
			
			if(Environment.getExternalStorageDirectory().exists()) {
				SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
			}else {
				SDCardRoot = Environment.getDownloadCacheDirectory().getAbsoluteFile();
			}
			
			String filename=albumName + ".png";   
			Log.i("Local filename:",""+filename);
			File file = new File(SDCardRoot,filename);
			if(file.createNewFile())
			{
				file.createNewFile();
			}                 
			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream inputStream = urlConnection.getInputStream();
			
			int totalSize = urlConnection.getContentLength();
			int downloadedSize = 0;   
			byte[] buffer = new byte[1024];
			int bufferLength = 0;
			
			if(inputStream != null) {
				while ( (bufferLength = inputStream.read(buffer)) > 0 ) 
				{                 
					fileOutput.write(buffer, 0, bufferLength);                  
					downloadedSize += bufferLength;                 
					Log.i("Progress:","downloadedSize:"+downloadedSize+"totalSize:"+ totalSize) ;
				}             
				fileOutput.close();
				if(downloadedSize==totalSize) mFilePath=file.getPath(); 
			}else {
				mFilePath = null;
			}
		} 
		catch (MalformedURLException e) {
			mFilePath=null;
			e.printStackTrace();
		} 
		catch (IOException e) {
			mFilePath=null;
			e.printStackTrace();
		}
		Log.i("filepath:"," "+mFilePath);
		return mFilePath;
	}
}
