package com.test.itunesmusicchartapp.asynctasks;

import com.test.itunesmusicchartapp.MainActivity;
import com.test.itunesmusicchartapp.util.Util;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Downloads the image from the url obtained
 * @author USER
 *
 */
public class DownloadImageAsyncTask extends AsyncTask<Void, Void, String> {
	private Context mContext;
	private String mImgURL;
	private String mAlbumName;
	private Util mUtil;
	private String mFilePath, mMessage;
	
	/**
	 * Constructor
	 * @param context
	 * @param imgURL
	 * @param albumName
	 */
	public DownloadImageAsyncTask(Context context, String imgURL, String albumName) {
		mContext = context;
		mImgURL = imgURL;
		mAlbumName  =albumName;
		mUtil = new Util(mContext);
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// downloads the image and stores it in the sdcard 
		// or downloads folder of the phone
		mFilePath = mUtil.downloadImageFromURL(mImgURL, mAlbumName);
		return mFilePath;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		// displays a message to the user as to what the status of the download is
		((MainActivity)mContext).showMessage(result, mMessage);
	}
}
