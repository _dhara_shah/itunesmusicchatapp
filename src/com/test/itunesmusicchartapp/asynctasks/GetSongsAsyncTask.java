package com.test.itunesmusicchartapp.asynctasks;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.xml.sax.InputSource;

import ae.javax.xml.bind.JAXBContext;
import ae.javax.xml.bind.JAXBException;
import ae.javax.xml.bind.Unmarshaller;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.test.itunesmusicchartapp.MainActivity;
import com.test.itunesmusicchartapp.R;
import com.test.itunesmusicchartapp.contentprovider.SongContentProvider;
import com.test.itunesmusicchartapp.db.SongTable;
import com.test.itunesmusicchartapp.models.Song;
import com.test.itunesmusicchartapp.models.TopSongs;

/**
 * Async task that fetches songs from the url: 
 * http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=20/xml
 * Parsing the xml using JAXB Parser
 * and storing the data using ContentProvider 
 * Called by {@MainActivity}
 * @author USER
 *
 */
public class GetSongsAsyncTask extends AsyncTask<Void, Void, TopSongs>{
	private Context mContext;
	private String mLimit;
	private ProgressDialog mProgressDialog;
	private String TAG = this.getClass().getSimpleName();
	
	/**
	 * Constructor
	 * @param context
	 * @param limit
	 */
	public GetSongsAsyncTask(Context context, int limit) {
		mContext = context;
		mLimit = String.valueOf(limit);
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = ProgressDialog.show(mContext, null, mContext.getString(R.string.loading));
	}
	
	@Override
	protected TopSongs doInBackground(Void... params) {
		try {
			URL url = new URL("http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=" + 
								mLimit+
								"/xml");
			InputSource is = new InputSource(url.openStream());
            is.setEncoding("ISO-8859-1");
            
            if(is != null) {
            	// parsing of the xml and storing of data into TopSongs Object
    			JAXBContext jaxbContext = JAXBContext.newInstance(TopSongs.class);
    			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    			TopSongs topSongs = (TopSongs)jaxbUnmarshaller.unmarshal(is);
    			return topSongs;
            }
		}catch (JAXBException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(TopSongs result) {
		super.onPostExecute(result);
		if(result != null) {
			// checks if data has been received
			// call made to store the data into the database
			if(result.getSongs() != null && 
					result.getSongs().size() > 0) {
				// insert into the db
				// after the creation of contentvalues
				ContentValues[] contentValues = new ContentValues[result.getSongs().size()];
				
				int index = 0;
				for(Song song: result.getSongs()) {
					contentValues[index] = new ContentValues();
					contentValues[index].put(SongTable.COL_SONG_TITLE, song.getTitle());
					contentValues[index].put(SongTable.COL_SONG_LINK, song.getSongLinks().get(0).getHref());
					contentValues[index].put(SongTable.COL_ALBUM_IMG, song.getImgUrl());
					index ++;
				}
				
				// bulk insert into the database
				if(contentValues.length > 0) {
					int rows = 
							mContext.getContentResolver().bulkInsert(SongContentProvider.CONTENT_URI, 
									contentValues);
					Log.i(TAG,"rows inserted : " + rows);
				}
			}
		}else {
			// there was no data obtained
			((MainActivity)mContext).displayMessage();
		}
		mProgressDialog.dismiss();
	}
}