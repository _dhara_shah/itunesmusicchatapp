package com.test.itunesmusicchartapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * This activity displays the link of the song
 * in a webview
 * @author USER
 *
 */
public class SongLinkActivity extends Activity{
	private WebView mWebView;
	private String mSongLink;
	private ProgressDialog mProgressDialog;

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.activity_finish_in_anim, R.anim.activity_finish_out_anim);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON); 
		this.setProgressBarVisibility(true);

		setContentView(R.layout.activity_song_detail);

		if(getIntent().getExtras() != null) {
			mSongLink = getIntent().getStringExtra("songLink");
		}

		mWebView = (WebView)findViewById(R.id.webView);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
		mWebView.setInitialScale(50);
	    mProgressDialog  = ProgressDialog.show(this, null, getString(R.string.loading));
	    mWebView.setWebViewClient(new MyWebViewClient());
		mWebView.loadUrl(mSongLink);
	}

	class MyWebViewClient extends WebViewClient {
		@Override
		public void onReceivedSslError (WebView view, SslErrorHandler handler,
				SslError error) {
			handler.proceed() ;
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url); // load url in webview
			return true;
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			if(mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
            }
		}
	}
}
