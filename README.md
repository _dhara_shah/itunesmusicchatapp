# README #

This application is developed in eclipse and Java sdk 1.6.0.

### What is this repository for? ###

* ITunesMusicChartApp is a simple application that is used to loads the music albums from the itunes link with the capability of downloading a song to play later on (offline)
* 1.0

### How do I get set up? ###

* This project can be imported into Eclipse easily
* Java is required
* No extra dependencies

### Who do I talk to? ###

* Dhara Shah